#ifndef USB_DEFS_H_
#define USB_DEFS_H_

#include "pico/types.h"

// these don't seem to be defined in usb.h but are used for proper endpoint configuration
#define USB_BUFFER_TYPE_CONTROL	0
#define USB_BUFFER_TYPE_ISO	1
#define USB_BUFFER_TYPE_BULK	2
#define USB_BUFFER_TYPE_INT	3

#define USB_REQ_GET_DIR(req)	((req)->bmRequestType & 0x80)
#define USB_REQ_GET_TYPE(req)	((req)->bmRequestType & 0x60)
#define USB_REQ_GET_RECIPIENT(req)	((req)->bmRequestType & 0x1f)

// transmission direction, OUT = host to device, IN = device to host
#define USB_DIRECTION_OUT	0x00
#define USB_DIRECTION_IN	0x80

// type identifier
#define USB_TYPE_STANDARD	0x00
#define USB_TYPE_CLASS		0x20
#define USB_TYPE_VENDOR		0x40

// recipient identifier
#define USB_RECIPIENT_DEVICE	0x00
#define USB_RECIPIENT_INTERFACE	0x01
#define USB_RECIPIENT_ENDPOINT	0x02
#define USB_RECIPIENT_OTHER	0x03
// NOTE:	when an endpoint is addressed, wIndex[3:0] is the endpoint number
//		when an interface is addressed, wIndex[7:0] is the interface number

// request codes used in bRequest
#define USB_REQ_GET_STATUS		0x00
#define USB_REQ_CLEAR_FEATURE		0x01
#define USB_REQ_SET_FEATURE		0x03
#define USB_REQ_SET_ADDRESS		0x05
#define USB_REQ_GET_DESCRIPTOR		0x06
#define USB_REQ_SET_DESCRIPTOR		0x07
#define USB_REQ_GET_CONFIGURATION	0x08
#define USB_REQ_SET_CONFIGURATION	0x09
#define USB_REQ_GET_INTERFACE		0x0a
#define USB_REQ_SET_INTERFACE		0x0b
#define USB_REQ_SYNCH_FRAME		0x0c

// descriptor types a host can GET or SET (comes in wValue along with descriptor index)
#define USB_DESC_TYPE_DEVICE		0x01
#define USB_DESC_TYPE_CONFIGURATION	0x02
#define USB_DESC_TYPE_STRING		0x03
#define USB_DESC_TYPE_INTERFACE		0x04
#define USB_DESC_TYPE_ENDPOINT		0x05
#define USB_DESC_TYPE_DEVICE_QUALIFIER	0x06
#define USB_DESC_TYPE_OTHER_SPEED_CONF	0x07
#define USB_DESC_TYPE_INTERFACE_POWER	0x08
#define USB_DESC_TYPE_HID		0x21
#define USB_DESC_TYPE_HID_REPORT	0x22

// endpoint types
#define USB_EP_TYPE_CONTROL		0x00
#define USB_EP_TYPE_ISOCHRONOUS		0x01
#define USB_EP_TYPE_BULK		0x02
#define USB_EP_TYPE_INTERRUPT		0x03

// class codes
#define USB_CLASS_CODE_HID		0x03

// macros to interpret the wValue field of GetDescriptor and SetDescriptor
#define USB_REQ_GET_DESC_TYPE(req)	(((req)->wValue >> 8) & 0xff)
#define USB_REQ_GET_DESC_INDEX(req)	((req)-> wValue & 0xff)


// request data structure used by the host to send data to a device or request some
struct usb_request {
	uint8_t bmRequestType;		// type of the request including direction (IN/OUT) and recipient (device, interface, endpoint)
	uint8_t bRequest;		// request identifier
	uint16_t wValue;		// request-dependent value
	uint16_t wIndex;		// depends on the request as well, may be, e.g., an interface or endpoint ID
	uint16_t wLength;		// number of data bytes following the request descriptor (OUT) or requested by the host (IN)
} __packed;

struct usb_descriptor_common {
	uint8_t	bLength;		// this is always the size of the descriptor alone. If the descriptor has others associated with it, like configuration, there's an additional wTotalLength field.
	uint8_t bDescriptorType;	// the descriptor type, assigned by USB-IF
} __packed;

struct usb_hid_descriptor {
	uint8_t bLength;		// descriptor size in bytes
	uint8_t bDescriptorType;	// descriptor type identifier
	uint16_t bcdHID;		// release of the HID spec supported
	uint8_t bCountryCode;		// only relevant for things with country-specific layout like keyboards
	uint8_t bNumDescriptors;	// number of subordinate descriptors, at least one Report descriptor
	uint8_t bReportDescriptorType;	// type of the first subordinate descriptor
	uint16_t wReportDescriptorLength;	// size, in bytes, of the first subordinate descriptor
	// if more subordinate descriptors are required, more {bDescriptor, wDescriptorLength} tuples have to be added.
} __packed;// 9 Byte

// device descriptor
struct usb_device_descriptor {
	uint8_t bLength;		// descriptor size in bytes
	uint8_t bDescriptorType;	// descriptor type identifier
	uint16_t bcdUSB;		// the version of the USB standard supported, e.g., 0x0101 -> 1.1
	uint8_t bDeviceClass;		// device class code (HID devices use 0 here and report class in the interface descriptors)
	uint8_t bDeviceSubClass;	// a sub class which further divides classes to group devices of similar kind (also 0 for HID)
	uint8_t bDeviceProtocol;	// can be used to specify a protocol used by the device
	uint8_t bMaxPacketSize0;	// maximum packet size supported by endpoint 0 (8, 16, 32 or 64)
	uint16_t idVendor;		// vendor ID, assigned by USB consortium
	uint16_t idProduct;		// product ID which can be used by a driver to select the proper code
	uint16_t bcdDevice;		// device release number, vendor-specific
	uint8_t iManufacturer;		// index of manufacturer name string
	uint8_t iProduct;		// index of product name string
	uint8_t iSerialNumber;		// index of serial number string
	uint8_t bNumConfigurations;	// number of possible configurations
} __packed;// 18 Byte

// there's also a device qualifier descriptor, however, it's only used by High-Speed devices.

// configuration descriptor
struct usb_configuration_descriptor {
	uint8_t bLength;		// descriptor size in bytes
	uint8_t bDescriptorType;	// descriptor type identifier
	uint16_t wTotalLength;		// length of the configuration descriptor + all interface, endpoint and, possibly,
					// report descriptors. This is the number of bytes the host will read after initially
					// reading just the configuration descriptor.
	uint8_t bNumInterfaces;		// number of interfaces this configuration uses, i.e., the number of interface descriptors
	uint8_t bConfigurationValue;	// the value the host adds to a Set Configuration message to select this configuration.
	uint8_t iConfiguration;		// here the index of a string describing this configuration can be  added.
	uint8_t bmAttributes;		// device attributes, like self/bus powered
	uint8_t bMaxPower;		// maximum power the device draws = bMaxPower * 2mA; when inserted, this is limited to xxx mA
} __packed;// 9 Byte

struct usb_interface_descriptor {
	uint8_t bLength;		// descriptor size in bytes
	uint8_t bDescriptorType;	// descriptor type identifier
	uint8_t bInterfaceNumber;	// interface index, zero-based!
	uint8_t bAlternateSetting;	// can be used to specify an alternate interface.
	uint8_t bNumEndpoints;		// number of endpoints used by this interface (and, hence, endpoint descriptors supplied)
	uint8_t bInterfaceClass;	// a class code, assigned by USB-IF
	uint8_t bInterfaceSubClass;	// a sub-class which can specify the device more precisely
	uint8_t bInterfaceProtocol;	// a protocol used by this interface
	uint8_t iInterface;		// index for retrieving a string describing this interface
} __packed;// 9 Byte

struct usb_endpoint_descriptor {
	uint8_t bLength;		// descriptor size in bytes
	uint8_t bDescriptorType;	// descriptor type identifier
	uint8_t bEndpointAddress;	// address and direction of this endpoint, e.g., EP1 IN
	uint8_t bmAttributes;		// the endpoints transfer type
	uint16_t wMaxPacketSize;	// maximum number of bytes per packet
	uint8_t bInterval;		// used to tell the host how often this endpoint should be probed for data.
					// Important for interrupt and isochronous endpoints. Value in ms.
} __packed;// 7 Byte

// There's no struct for string descriptors here as they're a bit special. The first one contains a list of language codes
// while the remaining ones contain actual strings. A string descriptor is formed by prepending the respective item with
// a bLength and bDescriptorType. Since each descriptor is specific to its content we can't actually define them here.
// The best way to go is use usb_descriptor_common, set the values, copy it to the send buffer and then copy the string
// or list of language codes behind it.

#endif
