// I/O functions like printf used for debug output
#include <stdio.h>
// string.h contains memcpy which we use to copy data between USB DPRAM and memory buffers
#include <string.h>
// use C wide characters (16 bit) for USB string descriptors
#include <wchar.h>
// required for initializing UART output
#include "pico/stdlib.h"
// USB data structures and registers
#include "hardware/structs/usb.h"
#include "hardware/regs/usb.h"
// for interrupt control register
#include "hardware/irq.h"
// for reset control
#include "hardware/resets.h"
// for set/clear register alternatives
#include "hardware/address_mapped.h"

// USB descriptor definitions
#include "usb_defs.h"
#include "nes_gamepad.h"

//#define USE_4BUTTONS_REPORT_TYPE

// unfortunately, wchar_t doesn't work properly.
// having const in the second place makes the strings constant
char const* stringDescriptors[] = {
	(const char[]){0x0409},	// first descriptor is the language code, English is used here. 16 Bit language ID defined by USB-IF
	"Golem.de",			// manufacturer
	"NESsi RetroPad",		// product name :)
	"00000001"			// product serial number
};

// here we store the current PID value of endpoint 0. It's required when sending transactions containing more
// than one data packets. To simplify handling an array is used although we don't need to track EP 1 PID.
uint8_t epPIDNext[2];

// variable to remember device address as we must only change it after the Status phase of the Set Address transaction has completed.
uint usbDevAddr = 0;
// flag to remember whether we have already set the device address.
bool usbDevAddrSet = false;
// flag to remember whether host has configured the device
bool usbConfigured = false;

struct usb_device_descriptor devDesc = {
	.bLength		= sizeof(struct usb_device_descriptor),
	.bDescriptorType	= USB_DESC_TYPE_DEVICE,
	.bcdUSB			= 0x0110,
	.bDeviceClass		= 0,// class and sub-class are 0 for HID devices in device descriptor.
	.bDeviceSubClass	= 0,// Class is defined in the interface descriptor.
	.bDeviceProtocol	= 0,
	.bMaxPacketSize0	= 64,
	.idVendor		= 0x0000,// I don't have a vendor ID...	
	.idProduct		= 0xb007,
	.bcdDevice		= 0,
	.iManufacturer		= 1,
	.iProduct		= 2,
	.iSerialNumber		= 3,
	.bNumConfigurations	= 1
};

struct usb_configuration_descriptor confDesc = {
	.bLength		= sizeof(struct usb_configuration_descriptor),
	.bDescriptorType	= USB_DESC_TYPE_CONFIGURATION,
	.wTotalLength		= sizeof(struct usb_configuration_descriptor)
				+ sizeof(struct usb_interface_descriptor)//we only have one interface, otherwise this would have to be multiplied by the number of interfaces
				+ sizeof(struct usb_endpoint_descriptor)// same applies here, but we only have one endpoint (EP0 doesn't have a descriptor).
				+ sizeof(struct usb_hid_descriptor),
	.bNumInterfaces		= 1,
	.bConfigurationValue	= 1,
	.iConfiguration		= 0,
	.bmAttributes		= 0x80,// bus powered
	.bMaxPower		= 50// 100 mA should be more than the Pico actually needs
};

struct usb_interface_descriptor ifDesc = {
	.bLength		= sizeof(struct usb_interface_descriptor),
	.bDescriptorType	= USB_DESC_TYPE_INTERFACE,
	.bInterfaceNumber	= 0,// zero-based!
	.bAlternateSetting	= 0,// TINA
	.bNumEndpoints		= 1,// we only use on interrupt IN endpoint
	.bInterfaceClass	= USB_CLASS_CODE_HID,// HID class 
	.bInterfaceSubClass	= 0,// no sub class, only relevant for keyboards and mice which can be boot interfaces as a hint to BIOS/UEFI
	.bInterfaceProtocol	= 0,// also just used by keyboards and mice to tell whether this is a keyboard or mouse
	.iInterface		= 0
};

struct usb_endpoint_descriptor ep1InDesc = {
	.bLength		= sizeof(struct usb_endpoint_descriptor),
	.bDescriptorType	= USB_DESC_TYPE_ENDPOINT,
	.bEndpointAddress	= USB_DIRECTION_IN | 0x01,
	.bmAttributes		= USB_EP_TYPE_INTERRUPT,
	.wMaxPacketSize		= 0x08,// we only have a tiny 3-Byte-report; for interrupt endpoints, the host uses this value to reserve bus time
	.bInterval		= 0x0a// poll this endpoint every 10 ms
};

#ifndef USE_4BUTTONS_REPORT_TYPE
// the report descriptor sequence of bytes
const char reportDescriptor[] = {
	0x05, 0x01,// Usage Page(Generic Desktop)
	0x09, 0x05,// Usage(Gamepad)
	0xa1, 0x01,// Collection(Application)
	0x15, 0x81,// Logical Minimum(-127)
	0x25, 0x7f,// Logical Maximum(127)
	0x09, 0x30,// Usage(X)
	0x09, 0x31,// Usage(Y)
	0x75, 0x08,// Report Size(8)
	0x95, 0x02,// Report Count(2)
	0x81, 0x02,// Input(Data, Variable, Absolut)
	0x75, 0x01,// Report Size(1)
	0x95, 0x02,// Report Count(2)
	0x15, 0x00,// Logical Minimum(0)
	0x25, 0x01,// Logical Maximum(1)
	0x19, 0x3d,// Usage Minimum(Start)
	0x29, 0x3e,// Usage Maximum(Select)
	0x81, 0x02,// Input(Data, Variable, Absolute)
	0x05, 0x09,// Usage Page(Button)
	0x19, 0x01,// Usage Minimum(Button 1)
	0x29, 0x02,// Usage Maximum(Button 2)
	0x81, 0x02,// Input(Data, Variable, Absolute)
	0xc0// End Collection
};
#else
// this is another version of the descriptor which treats start and select as buttons.
const char reportDescriptor[] = {
	0x05, 0x01,// Usage Page(Generic Desktop)
	0x09, 0x05,// Usage(Gamepad)
	0xa1, 0x01,// Collection(Application)
	0x15, 0x81,// Logical Minimum(-127)
	0x25, 0x7f,// Logical Maximum(127)
	0x09, 0x30,// Usage(X)
	0x09, 0x31,// Usage(Y)
	0x75, 0x08,// Report Size(8)
	0x95, 0x02,// Report Count(2)
	0x81, 0x02,// Input(Data, Variable, Absolut)
	0x75, 0x01,// Report Size(1)
	0x95, 0x04,// Report Count(4)
	0x15, 0x00,// Logical Minimum(0)
	0x25, 0x01,// Logical Maximum(1)
	0x05, 0x09,// Usage Page(Button)
	0x19, 0x01,// Usage Minimum(Button 1)
	0x29, 0x04,// Usage Maximum(Button 4)
	0x81, 0x02,// Input(Data, Variable, Absolute)
	0xc0// End Collection
};
#endif

// the HID class descriptor precedes the report descriptor
struct usb_hid_descriptor hidDesc = {
	.bLength			= sizeof(struct usb_hid_descriptor),
	.bDescriptorType		= USB_DESC_TYPE_HID,
	.bcdHID				= 0x0111,// the latest HID spec is 1.11
	.bCountryCode			= 0,// device isn' localized; if it was, we could put a country ID here
	.bNumDescriptors		= 1,// just the report descriptor follows
	.bReportDescriptorType		= USB_DESC_TYPE_HID_REPORT,
	.wReportDescriptorLength	= sizeof(reportDescriptor)
};

void setupEndpoints() {
	uint bufferOff = (uint32_t)usb_dpram->epx_data ^ (uint32_t)usb_dpram;
	// configure endpoint 1, it uses offset 0 as endpoint 0 doesn't have a control register
	// most features are left unused, the IN endpoint is just enabled, set up as an interrupt endpoint
	// and the buffer address set to the beginning of the configurable buffer area in DPRAM (DPRAM + 0x180).
	// It's very important to set EP_CTRL_INTERRUPT_PER_BUFFER, otherwise we don't have a chance to clear
	// the done bit.
	usb_dpram->ep_ctrl[0].in = EP_CTRL_ENABLE_BITS | EP_CTRL_INTERRUPT_PER_BUFFER |
					(USB_BUFFER_TYPE_INT << EP_CTRL_BUFFER_TYPE_LSB) |
					bufferOff;
	
	printf("EP 1 IN: %08x\n");
	
	
	epPIDNext[0] = 1;
	epPIDNext[1] = 0;
}

// a handy function to trigger transmission of a buffer. Data is copied and all necessary flags are set for the controller to send it.
void usbTransmitBuffer(uint8_t ep, const char* buffer, uint16_t bytes, bool lastBuffer) {
	uint txCtrl = 0;
	uint dstAddr;

	// do some boundary checks first
	if(ep > 2) {
		printf("endpoint %u isn't active, can't send\n", ep);
	} else if(bytes > 64) {
		printf("sorry, I can't send more than 64 bytes; %d requested\n", bytes);
	} else {
		dstAddr = usb_dpram->ep0_buf_a + (ep << 7);
		// we only use buffer 0 for simplicity
		if((buffer != NULL) && (bytes > 0))
			memcpy(dstAddr, buffer, bytes);
		
		txCtrl = USB_BUF_CTRL_AVAIL | USB_BUF_CTRL_FULL | ((epPIDNext[ep]) ? USB_BUF_CTRL_DATA1_PID : USB_BUF_CTRL_DATA0_PID) | ((lastBuffer) ? USB_BUF_CTRL_LAST : 0) | bytes;
		
		// NOTE: the USB controller's registers are mapped via usb_hw while buffer control resides in DPRAM!
		usb_dpram->ep_buf_ctrl[ep].in = txCtrl;
		
		// toggle PID
		epPIDNext[ep] ^= 1;
	}
	
//	printf("-0x%08x data -> %08x\n", txCtrl, dstAddr);
}

// an even more handy function which copies data from several sources into the transmit buffer
void usbTransmitBuffer_gather(uint8_t ep, uint nSources, const char** buffer, uint16_t* bytes, bool lastBuffer) {
	uint txCtrl = 0, totalBytes, i, off;
	
	for(i = 0, totalBytes = 0; i < nSources; ++i)
		totalBytes += bytes[i];

	// do some boundary checks first
	if(ep > 2) {
		printf("endpoint %u isn't active, can't send\n", ep);
	} else if(totalBytes > 64) {
		printf("sorry, I can't send more than 64 bytes; %d requested\n", totalBytes);
	} else {
		for(i = 0, off = 0; i < nSources; ++i) {
			// we only use buffer 0 for simplicity
			if((buffer != NULL) && (bytes[i] > 0))
				memcpy(usb_dpram->ep0_buf_a + (ep << 7) + off, buffer[i], bytes[i]);

			off += bytes[i];
		}
		
		txCtrl = USB_BUF_CTRL_AVAIL | USB_BUF_CTRL_FULL | ((epPIDNext[ep]) ? USB_BUF_CTRL_DATA1_PID : USB_BUF_CTRL_DATA0_PID) | ((lastBuffer) ? USB_BUF_CTRL_LAST : 0) | totalBytes;
		
		// NOTE: the USB controller's registers are mapped via usb_hw while buffer control resides in DPRAM!
		usb_dpram->ep_buf_ctrl[ep].in = txCtrl;
		
		// toggle PID
		epPIDNext[ep] ^= 1; 
	}
	
	printf("0x%08x\n", txCtrl);
}

// function to handle setup transactions (which start control transfers)
void handleSetupTransaction() {
	volatile struct usb_request *req = (volatile struct usb_request *) &usb_dpram->setup_packet;
	uint bRequest, bmRequestType, wLength, wValue;
	bRequest = req->bRequest;
	bmRequestType = req->bmRequestType;
	wLength = req->wLength;
	wValue = req->wValue;
	
	epPIDNext[0] = 1;// Setup requests always use PID 0, the first data packet (or status packet) has to use PID 1.
	
	// first check direction of data transfer; for OUT there's less to do
	if(USB_REQ_GET_DIR(req) == USB_DIRECTION_OUT) {
		bool doStatus = false;
		
		switch(bRequest) {
		case USB_REQ_SET_ADDRESS:
			// buffer the address; it is written to the USB controller's register after the Status stage
			// completes. That's the case when we receive the next buffer status interrupt.
			// The address the host assigned us is in the lower wValue
			usbDevAddr = wValue & 0x7f;
			
			// check whether the address is okay
			if(usbDevAddr > 127) {
				// no, it's not, something is wrong.
				usbDevAddr = 0;
			}
			
			printf("got address %d\n", usbDevAddr);
			
			doStatus = true;
			break;
		case USB_REQ_SET_CONFIGURATION:
			// as we only have one configuration there's not much to do here.
			usbConfigured = true;
			doStatus = true;
			break;
		}
		
		// Both transactions have no data phase, but to complete the transaction we have to send
		// a 0 Byte packet in the Status stage. This is done by putting 0 bytes of data into EP0 IN.
		if(doStatus)
			usbTransmitBuffer(0, NULL, 0, true);
		
		printf("OUT request, request: %02x type: 0x%02x\n", bRequest, bmRequestType);
	} else {
		char *sources[4];
		uint16_t bytes[4];
		
		// for IN requests, the host sends an empty OUT request in the Status stage.
		if(bRequest == USB_REQ_GET_DESCRIPTOR) {
			switch(USB_REQ_GET_DESC_TYPE(req)) {
			case USB_DESC_TYPE_DEVICE:
				// send the device descriptor to host
				usbTransmitBuffer(0, (char*)&devDesc, devDesc.bLength, true);
				printf("Device Descriptor sent\n");
				break;
			case USB_DESC_TYPE_CONFIGURATION:
				if(req->wLength == confDesc.bLength) {
					// send only configuration descriptor
					usbTransmitBuffer(0, (char*)&confDesc, confDesc.bLength, true);
				} else {
					// when the configuration descriptor is requested we must return, in this order:
					// configuration descriptor, interface descriptor, HID descriptor and endpoint descriptor.
					sources[0] = (char*)&confDesc;
					sources[1] = (char*)&ifDesc;
					sources[2] = (char*)&hidDesc;
					sources[3] = (char*)&ep1InDesc;
					bytes[0] = confDesc.bLength;
					bytes[1] = ifDesc.bLength;
					bytes[2] = hidDesc.bLength;
					bytes[3] = ep1InDesc.bLength;
					
					usbTransmitBuffer_gather(0, 4, sources, bytes, true);
				}
				printf("Configuration Descriptor sent\n");
				break;
			case USB_DESC_TYPE_STRING:
				// string descriptors are composed from the generic descriptor and the respective string.
				char buf[64];
				struct usb_descriptor_common *sd = (struct usb_descriptor_common*)buf;
				uint sid = USB_REQ_GET_DESC_INDEX(req);
				
				if(sid == 0) {
					buf[2] = stringDescriptors[0][0];
					buf[3] = stringDescriptors[0][1];
					
					sd->bLength = 4;
					sd->bDescriptorType = USB_DESC_TYPE_STRING;
				} else {
					uint strBytes = strlen(stringDescriptors[sid]);
					
					sd->bLength = (strBytes << 1) + 2;
					sd->bDescriptorType = USB_DESC_TYPE_STRING;
					
					for(uint i = 0; i < strBytes; ++i) {
						buf[(i << 1) + 2] = stringDescriptors[sid][i];
						buf[(i << 1) + 3] = 0;
					}
				}
				
				usbTransmitBuffer(0, buf, sd->bLength, true);
				printf("String Descriptor sent\n");
				break;
			case USB_DESC_TYPE_HID:
				usbTransmitBuffer(0, (const char*)&hidDesc, (req->wLength < hidDesc.bLength) ? req->wLength : hidDesc.bLength, true);
				break;
			case USB_DESC_TYPE_HID_REPORT:
				usbTransmitBuffer(0, (const char*)reportDescriptor, sizeof(reportDescriptor), true);
				break;
			}
		}
		
		printf("IN request, code: %02x type: 0x%x wValue: %d wLength: %d\n", bRequest, bmRequestType, wValue, wLength);
	}
}

// interrupt handler for USB interrupt; we check what caused the interrupt and do the appropriate thing.
void usbIntHandler() {
	// USB interrupt handler
	uint32_t status = usb_hw->ints;
	uint32_t handled = 0;
	
	//printf("int");

	// first check what caused the interrupt. There may be several causes.
	// Setup packet received, starting a control
	if(status & USB_INTS_SETUP_REQ_BITS) {
		handled |= USB_INTS_SETUP_REQ_BITS;
		// hw_clear_alias clears bits in registers atomically to avoid race conditions between the two ARM cores.
		// This isn't an issue here as we only use one core, but good to know. There are similar functions for set and xor
		// as well as XIP cache.
		hw_clear_alias(usb_hw)->sie_status = USB_SIE_STATUS_SETUP_REC_BITS;
		handleSetupTransaction();
	}

	// buffer status interrupt is triggered once a buffer completes.
	if (status & USB_INTS_BUFF_STATUS_BITS) {
		uint bufferBits, buffersClearMask = 0;
		handled |= USB_INTS_BUFF_STATUS_BITS;
		
		bufferBits = usb_hw->buf_status;
		
		// check which endpoint's status changed... it can only be 0 or 1.
		if(bufferBits & 0x01) {
			// endpoint 0 IN
			buffersClearMask |= 0x01;
			
			// Set Address ends with a Status phase which is a 0 byte IN packet. Once that's done, assign the new address.
			if((!usbDevAddrSet) && (usbDevAddr != 0)) {
				// Status stage complete, set new address in control register
				usb_hw->dev_addr_ctrl = usbDevAddr;
				usbDevAddrSet = true;
				
				printf("address set in controller\n");
			} else {
				usb_dpram->ep_buf_ctrl[0].out = USB_BUF_CTRL_AVAIL | ((epPIDNext[0]) ? USB_BUF_CTRL_DATA1_PID : USB_BUF_CTRL_DATA0_PID);
				
				epPIDNext[0] ^= 1;
			}
		}
		if(bufferBits & 0x02) {
			// endpoint 0 OUT, we'll only see this for Set Address and Set Configuration.
			buffersClearMask |= 0x02;
			
			// make buffer available for USB controller again
			usb_dpram->ep_buf_ctrl[0].out = USB_BUF_CTRL_AVAIL;
		}
		if(bufferBits & 0x04) {
			// endpoint 1 IN, that's where we put the reports.
			buffersClearMask |= 0x04;
		}
		
		// now clear all flags we handled. Every bit that's 1 in buffersClearMask is atomically cleared to 0 in the register.
		hw_clear_alias(usb_hw)->buf_status = buffersClearMask;
		
//		printf("ep %08x buffers done\n", buffersClearMask);
	}

	// a bus reset, we have to reset the device to unconfigured state and revert address to 0
	if(status & USB_INTS_BUS_RESET_BITS) {
		printf("BUS RESET\n");
		handled |= USB_INTS_BUS_RESET_BITS;
		hw_clear_alias(usb_hw)->sie_status = USB_SIE_STATUS_BUS_RESET_BITS;

		usbDevAddr = 0;
		usbDevAddrSet = false;
		hw_clear_alias(usb_hw)->dev_addr_ctrl = 0;
		usbConfigured = false;
	}

	if (status ^ handled) {
		printf("unhandled interrupt source from USB controller - how could this happen?\n");
	}
}

// configure the USB block.
void setupUsbDevice() {
	// reset the USB controller first and wait until this is done
	reset_block(RESETS_RESET_USBCTRL_BITS);
	unreset_block_wait(RESETS_RESET_USBCTRL_BITS);

	// clear the USB block's memory
	memset(usb_dpram, 0, sizeof(*usb_dpram));

	// enable the USB interrupt in the interrupt controller, which can be raised by several events
	// which we'll enable in the USB controller's interrupt enable register below.
	// Also set the handler function.
	irq_set_exclusive_handler(USBCTRL_IRQ, usbIntHandler);
	irq_set_enabled(USBCTRL_IRQ, true);

	// Mux the controller to the onboard usb phy
	usb_hw->muxing = USB_USB_MUXING_TO_PHY_BITS;// | USB_USB_MUXING_SOFTCON_BITS;

	// Force VBUS detect so the device thinks it is plugged into a host
	usb_hw->pwr = USB_USB_PWR_VBUS_DETECT_BITS | USB_USB_PWR_VBUS_DETECT_OVERRIDE_EN_BITS;

	// enable USB controller in device mode (device mode is implicit)
	usb_hw->main_ctrl = USB_MAIN_CTRL_CONTROLLER_EN_BITS;

	// now we tell the controller when to raise an interrupt:
	// when a buffer is completed (buff_status), when it detects a bus reset (which is part of device enumeration),
	// when a Setup packet is received which starts data transmission. The other sources aren't relevant.
	// Actually, we only need the status bit to know when it's okay to change the device address. But since all data we transmit
	// is less than 64 bytes and therefore we don't need to know when a buffer is finished since we're not using double buffering.
	// If we did, we would know that we can fill the next buffer once we receive the status interrupt.
	usb_hw->inte = USB_INTE_BUFF_STATUS_BITS |
			USB_INTE_BUS_RESET_BITS |
			USB_INTE_SETUP_REQ_BITS;

	// setup the endpoint control register(s)
	setupEndpoints();
	
	// every ep0 buffer completed sets BUFF_STATUS and hence generates an interrupt,
	// enable pull-up resistor to tell host this is a full speed device
	usb_hw->sie_ctrl = USB_SIE_CTRL_EP0_INT_1BUF_BITS | USB_SIE_CTRL_PULLUP_EN_BITS;
}

int main() {
	// gpState is updated by the gamepad ISR, gpStateLast stores the last state so we only put data
	// into the output buffer when there's a change.
	uint gpState = 0xff, gpStateLast = 0;
	uint8_t report[3];

#ifdef PICO_DEFAULT_LED_PIN
	gpio_init(PICO_DEFAULT_LED_PIN);
	gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);
#endif
	
	stdio_init_all();
	printf("USB Gamepad started.\n");
	setupUsbDevice();
    
	if(gamepadStart(pio0, 8, 6, 7, &gpState) != 0) {
		printf("Oops: failed to initialize gamepad!\n");
		
		while(1) {}
	}
	
	printf("init done, starting data generation loop.\n");
	
	while (true) {
		sleep_ms(20);
		
		//printf("0x%08x\n", usb_hw->ints);
		
		// gpStateLast is always inverted as the NES gamepad flags pressed buttons as 0 (buttons have a pull-up,
		// pressing them connects it to ground).
		if(gpState != ~gpStateLast) {
//			printf("%02x %02x\n", gpState, gpStateLast);

			gpStateLast = ~gpState;
			
			// now generate the report packet;
			// X and Y each have one byte, where 127 means motion in positive direction (up or right),
			// -127 means motion in negative direction (down or left).
			report[0] = (gpStateLast & 0x01) ? 0x7f : ((gpStateLast & 0x02) ? 0x81 : 0);// X axis; bit 0 is right, bit 1 left
			report[1] = (gpStateLast & 0x04) ? 0x7f : ((gpStateLast & 0x08) ? 0x81 : 0);// Y axis; bit 3 is down, bit 2 is up
			// the four buttons share one bytes as each requires a single bit only.
			// The layout depends on the report type.
#ifndef USE_4BUTTONS_REPORT_TYPE
			// start and select are reported separately in bits 0 and 1, A and B in bits 2 and 3.
			report[2] = ((gpStateLast & 0x10) >> 4) | ((gpState & 0x20) >> 4) | ((gpState & 0x80) >> 5) | ((gpState & 0x40) >> 3);
#else
			// start and select are reported as buttons 3 and 4 in bits 2 and 3, A and B in bits 0 and 1.
			report[2] = ((gpStateLast & 0x10) >> 2) | ((gpState & 0x20) >> 2) | ((gpState & 0x80) >> 7) | ((gpState & 0x40) >> 5);
#endif
			// now transfer the report to the endpoint 1 buffer.
			if(usbConfigured)
				usbTransmitBuffer(1, (const char*)report, 3, true);
		}
		
#ifdef PICO_DEFAULT_LED_PIN
		// blink the LED to indicate configuration status: off -> not enumerated, blinking -> address assigned, on: configured
		if(!usbDevAddrSet) {
			gpio_put(PICO_DEFAULT_LED_PIN, 0);
		} else {
			if(!usbConfigured) {
				gpio_put(PICO_DEFAULT_LED_PIN, !gpio_get(PICO_DEFAULT_LED_PIN));
			} else {
				gpio_put(PICO_DEFAULT_LED_PIN, 1);
			}
		}
#endif
	}
	
	return 0;
}
